#ifndef VIGNERE_H
#define VIGNERE_H
#include <iostream>
#include <string>
using namespace std;

class vigenere
{
public:
    string key;
    vigenere(string key);
    string encrypt(string text);
    string decrypt(string text);
};

#endif // VIGNERE_H
