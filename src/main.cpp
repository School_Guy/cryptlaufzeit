#include "ui/mainwindow.h"
#include <QApplication>

/// The main entry point for the application.
/// \param argc The number of elements within argv
/// \param argv An array of c-string pointers.
/// \return The return code of the application.
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    return a.exec();
}

/*
 * int main()
 * {
 * Vigenere cipher("VIGENERECIPHER");
 *
 * string original = "Beware the Jabberwock, my son! The jaws that bite, the claws that catch!";
 * string encrypted = cipher.encrypt(original);
 * string decrypted = cipher.decrypt(encrypted);
 *
 * cout << original << endl;
 * cout << "Encrypted: " << encrypted << endl;
 * cout << "Decrypted: " << decrypted << endl;
 * }
*/
