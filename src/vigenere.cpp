#include "vigenere.h"

/// This is the constructor for the vignere class. It takes the key to encrypt a text as its only argument.
/// \param key The key to encrypt the text with.
vigenere::vigenere(string key) {
        for(int i = 0; i < key.size(); ++i)
        {
          if(key[i] >= 'A' && key[i] <= 'Z')
            this->key += key[i];
          else if(key[i] >= 'a' && key[i] <= 'z')
            this->key += key[i] + 'A' - 'a';
        }
}

/// Encrypt a given text with Vignere.
/// \param text The text to encrypt.
/// \return The encrypted text.
string vigenere::encrypt(string text) {
        string out;

        for(int i = 0, j = 0; i < text.length(); ++i)
        {
          char c = text[i];

          if(c >= 'a' && c <= 'z')
            c += 'A' - 'a';
          else if(c < 'A' || c > 'Z')
            continue;

          out += (c + key[j] - 2*'A') % 26 + 'A';
          j = (j + 1) % key.length();
        }

        return out;
}

/// Decrypt a given text with Vignere.
/// \param text The text to decrypt.
/// \return The decrypted text.
string vigenere::decrypt(string text) {
        string out;

        for(int i = 0, j = 0; i < text.length(); ++i)
        {
          char c = text[i];

          if(c >= 'a' && c <= 'z')
            c += 'A' - 'a';
          else if(c < 'A' || c > 'Z')
            continue;

          out += (c - key[j] + 26) % 26 + 'A';
          j = (j + 1) % key.length();
        }

        return out;
}
