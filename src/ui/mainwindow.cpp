#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "../vigenere.h"
#include "../aes.h"
#include <chrono>
#include <ctime>
#include <QLineEdit>
#include <QMessageBox>

/// The constructor of the Main Application Window.
/// \param parent Unknown parameter
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

/// The destructor of the Main Application Window.
MainWindow::~MainWindow()
{
    delete ui;
}

/// When clicking in the menu bar on "File" --> "Beenden" this code is executed.
void MainWindow::on_actionBeenden_triggered()
{
    exit(0);
}

/// When the button "Berechnen" on the AES panel side is clicked, then this code is executed.
void MainWindow::on_pushButtonAesCalc_clicked()
{
    //Check if key and plaintext is given (if not -> display alert,if yes -> save in vars)
    QString key=ui->lineEditAesKey->text();
    QString plaintext=ui->lineEditAesPlaintext->text();
    if (key != "" && plaintext != "") {
        //Encryption
        //char array for key and plaintext; length is size_t type and length of char array
        size_t sizeKey = key.size();
        size_t sizePlain = plaintext.size();
        char *plainCharArr = new char [sizePlain];
        char *keyCharArr = new char [sizeKey];
        memcpy( keyCharArr, key.toStdString().c_str() ,sizeKey);
        memcpy( plainCharArr, plaintext.toStdString().c_str() ,sizePlain);
        aes AES;
        //Time measure start
        std::chrono::time_point<std::chrono::system_clock> start, end;
        start = std::chrono::system_clock::now();
        //Actual encryption
        sizePlain = AES.encrypt(&plainCharArr,sizePlain,keyCharArr);
        //Time measure end
        end = std::chrono::system_clock::now();

        std::chrono::duration<double> elapsed_seconds = end-start;
        std::time_t end_time = std::chrono::system_clock::to_time_t(end);

        //Rerun everything and get steps in between (optional)

        //Set values (because values are overwritten)
        ui->labelAesKeyRes2->setText(key);
        ui->labelAesPlaintextRes2->setText(plaintext);
        ui->labelAesCiphertextRes2->setText(QString::fromStdString(plainCharArr));

        //Decryption -> same as Encryption
        size_t sizeCipher = strlen(plainCharArr);
        std::chrono::time_point<std::chrono::system_clock> startDe, endDe;
        startDe = std::chrono::system_clock::now();
        sizeCipher = AES.decrypt(&plainCharArr,sizeCipher,keyCharArr);
        endDe = std::chrono::system_clock::now();
        std::chrono::duration<double> elapsed_secondsDe = endDe-startDe;
        std::time_t end_timeDe = std::chrono::system_clock::to_time_t(endDe);

        //Set values
        ui->labelAesTimeDecryption2->setText(QString::fromStdString(std::to_string(elapsed_secondsDe.count())) + "s");
        ui->labelAesTimeEncryption2->setText(QString::fromStdString(std::to_string(elapsed_seconds.count())) + "s");
    } else {
        //Display Dialog
        QMessageBox msgBox;
        msgBox.setText("Bitte fülle alle Felder aus!");
        msgBox.exec();
    }
}

/// When the button "Berechnen" on the Vignere panel side is clicked, then this code is executed.
void MainWindow::on_pushButtonVignereCalc_clicked()
{
    //Check if key and plaintext is given (if not -> display alert,if yes -> save in vars)
    QString key=ui->lineEditVignereKey->text();
    QString plaintext=ui->lineEditVignerePlaintext->text();
    if (key != "" && plaintext != "") {
        //Encryption
        string stdKey = key.toUtf8().constData();
        string stdPlaintext = plaintext.toUtf8().constData();
        vigenere Vignere(stdKey);
        //Time measure start
        std::chrono::time_point<std::chrono::system_clock> start, end;
        start = std::chrono::system_clock::now();
        //Encryption
        string ciphertext = Vignere.encrypt(stdPlaintext);
        //Time measure end
        end = std::chrono::system_clock::now();
        std::chrono::duration<double> elapsed_seconds = end-start;
        std::time_t end_time = std::chrono::system_clock::to_time_t(end);

        //Decryption -> same as Encryption
        std::chrono::time_point<std::chrono::system_clock> startDe, endDe;
        startDe = std::chrono::system_clock::now();
        string plaintextDe = Vignere.encrypt(ciphertext);
        endDe = std::chrono::system_clock::now();
        std::chrono::duration<double> elapsed_secondsDe = endDe-startDe;
        std::time_t end_timeDe = std::chrono::system_clock::to_time_t(endDe);

        //Set text to values
        ui->labelVignereResCipher2->setText(QString::fromStdString(ciphertext));
        ui->labelVignereResPlain2->setText(plaintext);
        ui->labelVignereResKey2->setText(key);
        ui->labelVignereTimeDecryption2->setText(QString::fromStdString(std::to_string(elapsed_secondsDe.count())) + "s");
        ui->labelVignereTimeEnryption2->setText(QString::fromStdString(std::to_string(elapsed_seconds.count())) + "s");
    } else {
        //Display Dialog
        QMessageBox msgBox;
        msgBox.setText("Bitte fülle alle Felder aus!");
        msgBox.exec();
    }
}
