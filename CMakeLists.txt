cmake_minimum_required(VERSION 3.12)

project(CryptLaufzeit)

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED 14)

set(CMAKE_CXX_FLAGS "-g -Og ${CMAKE_CXX_FLAGS}")

set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)
set(CMAKE_AUTOUIC ON)

set(CMAKE_INCLUDE_CURRENT_DIR ON)

add_compile_definitions(QT_DEPRECATED_WARNINGS)

find_package(Qt5 COMPONENTS Widgets REQUIRED)

add_executable(SOURCES
        src/main.cpp
        src/aes.cpp
        src/vigenere.cpp
        src/ui/mainwindow.cpp
        )

target_link_libraries(SOURCES Qt5::Widgets)