# CryptLaufzeit

This project is part of my work during school. I was told to create a program which compares the runntime of two
cryptographic algorithms. I choose AES and Vignere. Since this is an educational exercise I wanted to implement them
on my own and not use pre programmed optimized implementations.

The AES implementation was taken from: https://www.c-plusplus.net/forum/topic/148732/aes-test-und-meinung
